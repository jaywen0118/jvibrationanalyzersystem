﻿using JCLibGeneral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JVAS
{
    //命令/結果共用資料結構
    public class CommandStruct
    {
        //public byte ID { get; set; }
        public byte Code { get; set; }   //命令或狀態代碼 (1 byte)
        public List<byte> PB { get; set; }  //傳送命令或回傳結果之參數位元集合 (長度不一定)

        public CommandStruct()
        {
            Code = 0;   
            PB = new List<byte>();
        }

        public CommandStruct(byte cmd, byte tb0, byte tb1, byte tb2, byte tb3)
        {
            Code = cmd;
            PB = new List<byte>();
            PB.Add(tb0);
            PB.Add(tb1);
            PB.Add(tb2);
            PB.Add(tb3);
        }

        //Deep Copy
        public CommandStruct Clone()
        {
            CommandStruct cmd = new CommandStruct();
            cmd.Code = this.Code;
            for (int i = 0; i < this.PB.Count; i++)
            {
                cmd.PB.Add(this.PB[i]);
            }
            return cmd;
        }

        public void Clear()
        {
            Code = 0;
            PB.Clear();
        }
    }

    public class PacketStruct
    {
        public double Number { get; set; } //號碼牌
        public int Status { get; set; } //狀態碼(0:busy,1:done,-1:timeout,-2:error...)
        public byte ID { get; set; }    //命令 ID，固定為 255 (1 byte)
        public CommandStruct CMD { get; set; }  //命令格式
        public int TimeoutMS { get; set; }  //通訊逾時的時間(毫秒)

        public PacketStruct()
        {
            Number = 0;
            Status = 0;
            ID = 0;
            CMD = new CommandStruct();
            TimeoutMS = 1000;
        }

        //轉成命令封包位元陣列 (Tx string)
        public byte[] ToTX()
        {
            if ((this != null) && (this.CMD.PB.Count == 4))
            {
                byte[] cmd = new byte[9];
                cmd[0] = 37; //起始字元
                cmd[1] = 255;    //ID, 固定為 255
                cmd[2] = this.CMD.Code;
                cmd[3] = this.CMD.PB[0];
                cmd[4] = this.CMD.PB[1];
                cmd[5] = this.CMD.PB[2];
                cmd[6] = this.CMD.PB[3];
                cmd[7] = (byte)(cmd[0] + cmd[1] + cmd[2] + cmd[3] + cmd[4] + cmd[5] + cmd[6]);  //checksum
                cmd[8] = 35; //結束字元
                return cmd;
            }
            return null;
        }

        //轉成結果資料結構
        public void FormRX(double number, byte[] ret)
        {
            this.Number = number;
            if (ret != null && ret.Length >= 5)  //資料長度至少為 5 bytes
            {
                byte checksum = 0;
                for (int i = 0; i < (ret.Length - 2); i++)
                {
                    checksum = (byte)(checksum + ret[i]);
                }
                if (checksum == ret[ret.Length - 2])
                {
                    this.Status = 1;
                    this.ID = ret[1];
                    this.CMD.Code = ret[2];
                    for (int i = 3; i < (ret.Length - 2); i++)
                    {
                        this.CMD.PB.Add(ret[i]);
                    }
                }
                else
                {
                    this.Status = -2;   //CheckSum 失敗
                    this.ID = ret[1];
                    this.CMD.Code = ret[2];
                    this.CMD.PB.Clear();    //清空參數內容
                }
            }
            else
            {
                this.Status = -3;   //回傳值為空或資料長度為 0
                this.ID = 255;
                this.CMD.Code = 0;
                this.CMD.PB.Clear();    //清空參數內容
            }
        }
    }


    public class JVASCommunicator
    {
        private JComport VAS = null;
        private JWorkerThread Worker = null;
        private Queue<PacketStruct> REQ = new Queue<PacketStruct>();
        private List<PacketStruct> RET = new List<PacketStruct>();
        private static object reqLocker = new object();
        private static object retLocker = new object();
        private JActionTask taskJob = new JActionTask();
        private double currentNumber = -1;
        private JTimer TM = new JTimer();

        public JVASCommunicator()
        {
            VAS = new JComport();
            Worker = new JWorkerThread(Job);
        }

        public bool Start(byte comport)
        {
            bool bRet = false;
            if (VAS != null)
            {
                if (VAS.Open(comport, 35))
                {
                    if (Worker != null)
                    {
                        taskJob.Reset();    //Reset Job Task
                        Worker.Start();
                        bRet = true;
                    }
                }
            }
            return bRet;
        }

        public void Stop()
        {
            if (Worker != null)
            {
                Worker.Stop();
            }
            if (VAS != null)
            {
                VAS.Dispose();
            }
        }

        public double SetRequire(CommandStruct cmd, int timeout=1000)
        {
            if ((cmd != null) && (cmd.Code > 0) && (cmd.PB.Count >= 4))
            {
                PacketStruct req = new PacketStruct();
                req.Number = Convert.ToInt64(DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds);
                req.ID = 255;   //固定為 255
                req.Status = 0;
                req.CMD = cmd.Clone();  //Deep copy a CommandStruct object.
                req.TimeoutMS = timeout;
                lock (reqLocker)
                {
                    REQ.Enqueue(req);   //將 req 加入 REQ queue 中
                }
                return req.Number;
            }
            return 0;
        }

        public PacketStruct GetResult(double number)
        {
            PacketStruct ret = null;
            lock(retLocker)
            {
                int idx = RET.FindIndex(x => (x.Number == number));
                if (idx >= 0)
                {
                    ret = RET[idx]; //將結果指定給 ret
                    RET.RemoveAt(idx);  //將結果從 RET List 中移除
                }
            }
            return ret;
        }

        private PacketStruct GetRequire()
        {
            PacketStruct req = null;
            lock (reqLocker)
            {
                if (REQ.Count > 0)
                {
                    req = REQ.Dequeue();
                }
            }
            return req;
        }

        private void SetResult(PacketStruct ret)
        {
            lock (retLocker)
            {
                RET.Add(ret);
            }
        }

        private void SaveLog(string type, byte[] tr)
        {
            if (tr == null)
            {
                JLogger.LogDebug("VasComm", $"{type} : Timeout !");
            }
            else
            {
                var msg = ""; System.Text.Encoding.Default.GetString(tr);
                for (int i = 0; i < tr.Length; i++)
                {
                    msg += $"{tr[i]} ";
                }
                JLogger.LogDebug("VasComm", $"{type} : {msg}");
            }
        }

        private void Job()
        {
            switch (taskJob.Value)
            {
                case 0: //從 REQ queue 前端取出一個項目，送出命令
                    {
                        PacketStruct req = GetRequire();
                        if (req != null)
                        {
                            byte[] tx = req.ToTX();
                            if (tx != null)
                            {
                                currentNumber = req.Number;
                                TM.Reset(req.TimeoutMS);
                                VAS.SendData(tx);
                                SaveLog("SEND",tx);
                                taskJob.Next();
                            }
                        }
                    }
                    break;
                case 1: //接收結果，並加至 RET List 中
                    {
                        byte[] rx = new byte[9];    //先建立一長度為 9 的 byte 陣列，於 VAS.GetResult() 中會依實際需求調整陣列大小
                        if (VAS.GetResult(ref rx))
                        {
                            SaveLog("RECEIVE", rx);
                            PacketStruct ret = new PacketStruct();
                            if (ret != null)
                            {
                                ret.FormRX(currentNumber, rx);
                                SetResult(ret);
                                taskJob.Reset();
                            }
                        }
                        else
                        {
                            if (TM.Start())
                            {
                                SaveLog("RECEIVE", null);
                                PacketStruct ret = new PacketStruct();
                                ret.Number = currentNumber;
                                ret.Status = -1;  //Timeout
                                SetResult(ret);
                                //Timeout 1000ms
                                taskJob.Reset();
                                //TODO : 此 Timeout 作法會有漏洞，假如裝置在收到command後，超過
                                //Timeout時間才回傳結果，則會發生結果錯置的問題(配錯號碼牌了)。
                            }
                        }
                    }
                    break;
            }
        }
    }
}
