﻿using JCLibGeneral;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JVAS
{
    public delegate void UpdateScreenInfoHandler(int type/*, int ret, List<int> data*/);

    public class JVASReporter : IDisposable
    {
        private UpdateScreenInfoHandler UpdateScreenInfo = null;
        private JVASCommunicator COMM = null;
        private CommandStruct CMD = null;
        private CommandStruct RET = null;
        private JWorkerThread Reporter = null;
        private JActionFlag _CommunicateFlag = new JActionFlag();
        private JActionTask taskReport = new JActionTask();
        private int _timeout = 1000;
        private Stopwatch _stopwatch = new Stopwatch();
                
        public double ReqNumber { get; private set; }
        public int Progress { get; private set; }
        public List<int> VasData { get; private set; }
        public int VasResult { get; private set; }

        public JVASReporter(JVASCommunicator comm, UpdateScreenInfoHandler updater)
        {
            COMM = comm;
            UpdateScreenInfo = updater;

            this._CommunicateFlag.Reset();
            taskReport.Reset();
            RET = new CommandStruct();
            VasData = new List<int>();
            this.ReqNumber = 0;
            this._stopwatch.Reset();

            Reporter = new JWorkerThread(Report);
            Reporter.Start();
        }

        public void Dispose()
        {
            if (Reporter != null)
            {
                Reporter.Stop();
            }
        }

        private void Report()
        {
            try
            {
                if (this._CommunicateFlag.IsDoIt())
                {
                    VasResult = ReadData();
                    if (VasResult != 0)
                    {
                        this.Progress = 100;
                        this._stopwatch.Stop();
                        this._CommunicateFlag.Done();
                    }
                    //更新畫面資訊                            
                    UpdateScreenInfo(CMD.Code/*, this.VasResult, this.VasData*/);
                }
            }
            catch (Exception e)
            {
                //...
            }
        }

        private int ReadData()
        {
            int iRet = 0;   //執行中
            JActionTask Task = taskReport;
            switch (Task.Value)
            {
                case 0:
                    {
                        Task.Next();
                    }
                    break;
                case 1:     //判斷連線與命令是否正確
                    {
                        if (COMM != null)
                        {
                            if (CMD != null)
                            {
                                if (CMD.Code != 0)
                                {
                                    this._stopwatch.Start();
                                    Task.Next();
                                }
                                else
                                {
                                    iRet = -97; //通訊命令錯誤
                                    Task.Next(99);
                                }
                            }
                            else
                            {
                                iRet = -98; //通訊命令未建立
                                Task.Next(99);
                            }
                        }
                        else
                        {
                            iRet = -99; //通訊元件未建立
                            Task.Next(99);
                        }
                    }
                    break;
                case 2:     //送出命令
                    {
                        this.Progress = ((Convert.ToInt32(this._stopwatch.ElapsedMilliseconds) * 100) / this._timeout);
                        this.ReqNumber = COMM.SetRequire(CMD, this._timeout);   //設定需求命令格式，並取的號碼牌
                        if (this.ReqNumber > 0)
                        {
                            Task.Next();
                        }
                        else
                        {
                            iRet = -96;  //命令格式錯誤，不送了                            
                            Task.Next(99);
                        }
                    }
                    break;
                case 3:     //取得結果
                    {
                        this.Progress = ((Convert.ToInt32(this._stopwatch.ElapsedMilliseconds) * 100) / this._timeout);
                        PacketStruct packetRet = COMM.GetResult(this.ReqNumber);
                        if (packetRet != null)
                        {                            
                            if (packetRet.Status > 0)
                            {
                                //通訊成功取得回傳值
                                RET = packetRet.CMD.Clone();    //Deep copy a CommandStruct Object                                
                                Task.Next();
                            }
                            else
                            {
                                iRet = packetRet.Status;    //通訊失敗(-1:Timeout, -2:CheckSum失敗, -3:回傳值為空或資料長度為 0)
                                Task.Next(99);
                            }
                        }
                    }
                    break;
                case 4:
                    {
                        this.Progress = ((Convert.ToInt32(this._stopwatch.ElapsedMilliseconds) * 100) / this._timeout);
                        if (ConvertRetToData(CMD.Code))
                        {
                            iRet = 1;   //完成
                            Task.Next(99);
                        }
                        else
                        {
                            iRet = -95;  //結果陣列資料轉換失敗
                            Task.Next(99);
                        }
                    }
                    break;
                case 99:     //Done
                    {
                        //IDLE here
                    }
                    break;
            }
            return iRet;
        }

        private bool ConvertRetToData(int code)
        {
            bool bRet = false;
            switch (code)
            {
                case 61:
                    {
                        for (int i = 0; i < RET.PB.Count; i++)
                        {
                            int data = RET.PB[i];
                            VasData.Add(data);
                        }
                        bRet = true;
                    }
                    break;
                case 64:
                case 65:
                    {
                        for (int i = 2; i < RET.PB.Count; i += 2)
                        {
                            int data = RET.PB[i] + (RET.PB[i + 1] * 256);
                            VasData.Add(data);
                        }
                        bRet = true;
                    }
                    break;
            }
            return bRet;
        }

        public bool Trigger(CommandStruct cmd, int ms = 1000)
        {
            if (this._CommunicateFlag.IsDoing())
            {
                return false;
            }

            #region Setting & Reset

            CMD = cmd.Clone();  //Deep copy a CommandStruct object.
            RET.Clear();
            this.VasData.Clear();
            this.VasResult = 0;
            this.Progress = 0;
            this._timeout = ms;
            taskReport.Reset();
            this._stopwatch.Reset();

            #endregion

            this._CommunicateFlag.DoIt();
            return true;
        }

    }
}
