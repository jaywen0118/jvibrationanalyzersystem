﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JVAS
{
    public partial class MainForm : Form
    {
        private JVASCommunicator VasComm = null;
        private JVASReporter VasReporter = null;
        private bool _VASConnected = false;

        private void UpdateScreenInfoInvoke(int type/*, int ret, List<int> data*/)
        {
            this.Invoke(new UpdateScreenInfoHandler(UpdateScreenInfo), type/*, ret, data*/);
        }

        private void UpdateScreenInfo(int type/*, int ret, List<int> data*/)
        {
            int ret = VasReporter.VasResult;
            List<int> data = VasReporter.VasData;
            int progress = VasReporter.Progress;
            switch (type)
            {
                case 64:    //Waveform
                    {
                        lbChartType.Text = "Waveform";
                        if (ret != 0)
                        {
                            lbStatus.Text = $"Return Code : {ret}, Data Length = {data.Count}";
                            if (data.Count > 0)
                            {
                                for (int i = 0; i < data.Count; i++)
                                {
                                    chart1.Series[0].Points.AddY(i);
                                    chart1.Series[0].Points[i].YValues[0] = data[i];
                                }
                            }
                        }
                        progressBar1.Value = Math.Max(0, Math.Min(100, progress));
                    }
                    break;
                case 65:    //Spectrum
                    {
                        lbChartType.Text = "Spectrum";
                        if (ret != 0)
                        {
                            lbStatus.Text = $"Return Code : {ret}, Data Length = {data.Count}";
                            if (data.Count > 0)
                            {
                                for (int i = 0; i < data.Count; i++)
                                {
                                    chart1.Series[0].Points.AddY(i);
                                    chart1.Series[0].Points[i].YValues[0] = data[i];
                                }
                            }
                        }
                        progressBar1.Value = Math.Max(0, Math.Min(100, progress));
                    }
                    break;
            }
        }

        public MainForm()
        {
            InitializeComponent();

            // Get a list of serial port names.
            string[] ports = SerialPort.GetPortNames();
            foreach (string name in ports)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(name);
                comToolStripMenuItem.DropDownItems.Add(item);
            }

            _VASConnected = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in comToolStripMenuItem.DropDownItems)
            {
                item.Click += new System.EventHandler(this.ComPortClick);
            }

            VasComm = new JVASCommunicator();
            if (VasComm != null)
            {
                VasReporter = new JVASReporter(VasComm, UpdateScreenInfoInvoke);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (VasReporter != null)
            {
                VasReporter.Dispose();
            }
            if (VasComm != null)
            {
                VasComm.Stop();
            }
        }

        private void ComPortClick(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in comToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }
            string portName = ((ToolStripMenuItem)sender).Text.Substring(3);    //去除開頭的COM

            if (_VASConnected)
            {
                VasComm.Stop();
                _VASConnected = false;
            }

            try
            {
                byte PortNumber = Convert.ToByte(portName);

                if (VasComm != null)
                {
                    if (VasComm.Start(PortNumber))
                    {
                        _VASConnected = true;
                        ((ToolStripMenuItem)sender).Checked = true;
                        MessageBox.Show("VAS-03U connected.");
                    }
                    else
                    {
                        MessageBox.Show("VAS-03U connect fail!");
                    }
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Comport格式錯誤 - 不是由後面接著一連串數字 (0 到 9) 的任意符號所組成。");
            }
            catch (OverflowException)
            {
                MessageBox.Show("Comport值超出範圍 - 代表小於 MinValue 或大於 MaxValue 的數字。");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!this._VASConnected)
            {
                MessageBox.Show("VAS未連線!");
                return;
            }
            bool bRet = false;
            if (rbWaveform.Checked)
            {
                bRet = VasReporter.Trigger(new CommandStruct(64, 0, 0, 0, 1), 30000);
            }
            else if(rbSpectrum.Checked)
            {
                bRet = VasReporter.Trigger(new CommandStruct(65, 1, 0, 0, 0), 30000);
            }

            if (bRet)
            {
                chart1.Series[0].Points.Clear();
            }
            else
            {
                MessageBox.Show("VAS trigger fail !");
            }
        }
    }
}
